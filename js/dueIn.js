---
---

var doFade = function (dueID, due) {
  
  // Convert to strings...
  var due = "" + due;
  var isInClass = "" + isInClass;
  
  
  if (isValidDueDate(due)) {
    var today     = moment();
    var todayString = today.format("ddd, MMM Do");
    var m = moment();
    
    m = parseDueDate(due);
  
    var diff = m.diff(today, 'days');
    var diffm = m.diff(today, 'minutes');
    

    // Do Fade?
    if ((diff <= 0) && (diffm < 0)) {
      //console.log("Setting fade-" + dueID);
      $("#" + "fade-" + dueID).css({
        "opacity" : .3,
      });
    
    }
  }
};

var dueIn = function (dueID, due, isInClass, prefix, scale) {
  //console.log("dueIn");
  //console.log("id " + dueID);
  //console.log("due " + due);
  //console.log("scale " + scale);
  
  // Convert to strings...
  var due = "" + due;
  var isInClass = "" + isInClass;
  
  
  if (isValidDueDate(due)) {
    var today     = moment();
    var todayString = today.format("ddd, MMM Do");
    var m = moment();
    
    m = parseDueDate(due);
  
    var diff = m.diff(today, 'days');
    var diffm = m.diff(today, 'minutes');
    
    var difference = prefix + " ";
    difference += m.from(moment());
    differenceHTML = "";
    
    //console.log(dueID + "-" + due + " diff: " + diff + " diffm: " + diffm);
    
    if (isInClass == "true") {
      // console.log("Categories: " + categories);
      differenceHTML += "<span style='font-size: " 
                    + scale + ";'"
                    + "class = 'label label-info pull-right'>"
                    + "in class"
                    + "</span>";
    } else {
        if ((diff < 7) && ((diff != 0) && (diffm > 0))) {
          star = getIcon("star");
          differenceHTML += "<span style='font-size: small; color: {{site.starcolor}};'>" + star + " TODO " + star + "</span>";
        }
        
        differenceHTML += "<span style='font-size: " + scale + ";' class='label " + 
          getRangeColor(m) + 
          " pull-right'>" +
          difference + 
          "</span>";
    } //end if/else in class

    } else { //Invalid due date.
      console.log ("Invalid due date: " + due + "-" + dueID);
    }// end isValidDueDate
  
  
  $("#" + dueID).html(differenceHTML);
};

var prettyDate = function (elem, due, format) {
  m = parseDueDate(due);
  $("#" + elem).html(m.format("ddd, MMM Do"));
};