---
title: Electricity and Electronics Syllabus
resource: local
layout: default
gavel: "<i class='fa fa-fixed fa-gavel'></i>"
---

{% include twocol label="Course" value=site.name %}
{% include twocol label="Number" value=site.number %}
{% include twocol label="Instructor" value="Matt Jadud" %}
{% include twocol label="Office" value="Danforth Tech 102B" %}
{% include twocol label="Phone" value=site.phone %}
{% include twocol label="Email" value="jadudm" %}


{% callout info %}
This syllabus may be amended, edited, or otherwise updated if deemed necessary by the instructor.
{% endcallout %}

## Why should you care about {{site.number}}?

What is electricity, and how can I safely work with it? How do I design and build electrical systems? How do I sense and automate the world around me using low-cost, low-power computing systems? Ten years ago, answering these questions was the provenance of electrical engineers; today, it is possible for anyone to develop the skills and tools to design, build, and program sensing and automation systems to monitor and control the world around them. 

## How will this course help you succeed?

As a student of technology and computing, understanding digital electronic systems is essential to your work. As someone who enjoys making things, debugging them when they don't work, and solving interesting problems, you will have the opportunity to develop and practice those life-long skills hands-on in this course. By the end of the course, you will have been launched down a path that lets you wear many hats, and explore the following questions for the rest of your life. As a...


* **MAKER**: How do I use the tools of the global electronics maker community to create, communicate, and collaborate on electrical systems?

* **DESIGNER**: What knowledge of electronic components (and how they come together) do I need to design circuits to sense and automate the world around me?

* **COMMUNICATOR**: How do I interpret and create the many textual and visual representations of electronic circuits, and use that information to build circuits that work?

* **PROGRAMMER**: How can I prepare myself to program systems that sense and automate the world around me, even if I've never programmed a computer before?

* **THEOR**: What are the connections between the theories of electricity and electronics to our daily lives?

* **INVENTOR**: What questions can I ask about myself and the world around me, now and in the future, that sensing and automation systems can help me answer?

## Where can I look for information?

Anywhere! We will be using a combination of resources this term. We will use the course text, but will also rely heavily on resources found online (textual and video). An important part of this course is to help you learn to leverage many diverse resources and extract from them the essential information you need to solve the challenge you are investigating at the moment—this is an essential skill that we will practice throughout the term.

## How will you succeed in this course?

* **PARTICIPATE**. You are expected to be present regularly (meaning, always), and actively engage in the reading, conversation, and practical work of the course. More than just "showing up," however, active participation means that you are questioning, making connections, and thinking about where the material you are learning fits in with what you already know, as well as what you are learning in other courses. 

* **COMMUNICATE**. I strive to answer questions promptly, and have set up a forum where you can ask questions of myself, the TAs, and the rest of the class. **The forum should be your preferred mode of asking questions about the content of the course**. Learning to interact professionally in such forums is a good idea, as they are a common tool for many maker communities online today.

* **ASPIRE**. You aren't going to learn by doing the bare minimum. Dive in, have fun, and do your best to do your best. By striving for excellence, you push yourself to master new ideas and get the most from your learning. Instead of reading just one source, read three. Instead of starting your work the night before it is due, start the day it is assigned. Instead of getting frustrated and spending hours banging your head against the desk, ask a question early... or, help answer someone else's question!

## How will you and I evaluate your progress?

We will have a shared document that captures your achievement of the [learning objectives](learningobjectives.html) for this course. You should be roughly 1/2 way through the learning objectives at the midterm, and you should have completed all of them by the end of the term. If your completion rate is too low (meaning, you're failing to finish assignments, submit revisions, or generally doing work of a low quality), we will meet to discuss your work and your strategy for getting back on track towards completion and excellence.

More details follow (below) regarding how your efforts will be measured and assessed in this course.

## Course Learning Goals 

If this was the *only* course in electricity and electronics you take at Berea College, and you never touch or study the subject again while you are a student, what questions do I hope you will be able to answer 10 years from now?

1. <a name="safe"></a>**How can I be safe in everything I do?** <br/>

1. <a name="comms"></a>**How do I communicate, with clarity, the results of my efforts?** <br/>

1. <a name="design"></a>**How can I design electrical systems to sense and interact with the world?** <br/>

1. <a name="build"></a>**How do I build and program those systems?** <br/>

Course goals provide the "big picture" overview; our learning objectives are the measurable steps towards each of these goals.

# Learning Objectives

Learning objectives are the assessable points of learning in the course. The number of these learning objectives you complete is what determines your final grade in this course.

These objectives are subject to change. In particular, the "jury is still out" on objectives marked with a {{page.gavel}}.

1. [Craft: Fabrication and Safety](#craft)

1. [Theory of Electronics](#theory)

1. [Components and Build](#build)

1. [Learning and Justice](#world)

## Craft: Fabrication and Safety

<a name="craft"></a>

This material will be covered over the course of the entire term, and will be assessed through quizzes, exams, structured reporting, and direct observation.

### Level 1

1. Demonstrate understanding of basic breadboarding practice, including the use of wire strippers, crimpers, and related tools.

1. Use Fritzing for breadboard diagrams of basic circuits.

1. Identify basic components in schematic form.

### Level 2

1. Demonstrate (through written material and practical demonstration) safe use of a soldering iron.

1. Use Fritzing for schematic capture.

1. Identify basic components in PCB form.

1. Build basic circuits from a schematic.

1. Use a multimeter for basic diagnostics (continuity, voltage, current).

1. Discuss and demonstrate good multimeter safety.

### Level 3

1. Lay out single-sided PCBs in Fritzing and cut on a PCB mill.

1. Use an oscilloscope for basic diagnostics of oscillating circuits. {{page.gavel}}

1. Reverse-engineer circuit schematics from a PCB.

1. Interpret datasheets for basic data (eg. maximum safe voltage and current limits).

## Theory of Electronics

<a name="theory"></a>

### Level 1

1. Use V=IR to calculate whether circuits are safe for components and/or people.

1. Use P=IV to calculate power consumed by a device.

1. Read and interpret basic component values (resistors, capacitors).

1. Know safe limits for voltage and current (for people).

### Level 2

1. Use V=IR and P=IV together to solve for unknowns.

1. Demonstrate understanding of series/parallel reductions for resistance, capacitance, and voltage.

1. Know basic truth tables.

1. Demonstrate parallel/series switch networks and relate them to logic systems.

1. Build and demonstrate working pull-up and pull-down resistor configurations.

### Level 3

1. Build and calculate appropriate voltage dividers for analog sensor use.

1. Understand basic control theories (closed-loop, open-loop, PID). {{page.gavel}}

1. Demonstrate basic statistical knowledge as applied to measurements — mean, median, mode, error, etc.

1. Demonstrate calibration or mapping between values.

## Components and Build

<a name="build"></a>

This category of learning outcomes is subject to a fair bit of revision. Specifically, some of these outcomes are about programming, and it might be nice to see those outcomes in their own category... even if it is only to clean this up a bit.

### Level 1

1. Demonstrate knowledge of basic tools (soldering iron, solder sucker, wire stripper).

1. Demonstrate knowledge of passive components:
    * Resistors
    * Capacitors
    * Diodes

1. Build circuits from multiple kinds of instructions (schematic, text) using passive components.

### Level 2

1. Demonstrate the ability to actuate (on, off) components using code.

1. Demonstrate ability to read the state of binary inputs (eg. buttons) and use conditionals in code.

1. Demonstrate knowledge of switching components:
    * Transistors
    * Solid-state relays
    * Mechanical relays
    * Buttons/DIPs/Switches

### Level 3

1. Demonstrate knowledge of analog input and output

1. Demonstrate the ability to develop subroutines for code reuse. {{page.gavel}}

1. Demonstrate knowledge and use of basic actuators:
    * Servos
    * DC Motors
    * Steppers
    * Solenoids

1. Demonstrate knowledge and use of basic analog sensors:
    * Temperature
    * Light
    * Force
    * Moisture

1. Demonstrate ability to compute over values (write expressions) in code.

## Learning and Justice {{page.gavel}}

<a name="world"></a>

An important theme of many of the assignments in the course is one of building a foundation you can use to continue the exploration of electronics on your own. Because of this, we will (at times) be explicit in our discussion of learning, our reflection on our own learning, and the societal pressures that might stand in the way of our success as learners.

In other words, some of our reading discussion will center on questions of race, sex, gender, and socioeconomic status, and the challenges we might encounter throughout our lives as students of electronics.

### Level 1

1. Read and reflect (in writing) meaningfully on videos and blog-style content on the topic of learning.

### Level 2

1. Discuss, through semi-structured interview (or structured journal) on topics of success and challenge regarding learning in electricity and electronics.

1. Discuss respectfully, in small-group contexts, on topics of race, gender, and socioeconomic status in the context of learning in electronics and STEM-related disciplines.

1. Practice intense listening.
 
### Level 3

1. Listen, intensely and with empathy, to peers in small- and large-group settings.

1. Meta-reflect, in writing, on personal strengths and weaknesses in learning, by reflecting back over previous writings and documents regarding the practice and learning of electronics.

1. Identify directions for further growth and study in electricity and electronics.

{% comment %}
## Habits of Practice

These are not learning objectives *per se*, but instead represent practices that you are expected to engage in all term.

1. Engage in question asking and answering in [Q.berea]({{site.data.urls.qberea}}), our course discussion tool.

1. Get together with your learning team(s) at least once per week outside of class. {{page.gavel}}

1. Journal regarding your learning successes and challenges. (This might also be thought of as a *learning trajectory log*, where you reflect on what objectives you are making progress on, and so on.)

These will likely be incorporated explicitly in assignments.
{% endcomment %}

# Assessment

{% callout danger %}
These criteria may need to be updated at the midterm (or later), as the Spring of 2015 is the first use of these standards in this course.
{% endcallout %}

Completion of the learning objectives accounts for 90% of your final assessment in {{site.name}}. Each objective can be completed at one of two levels:

* **Excellent**. An objective can be completed with excellence. This will be marked as an "E". 

* **Proficient**. An objective can be completed with proficiency. This will be marked as a "P." It is possible, in some cases, to revise work to move from proficiency to excellence.

Otherwise, an objective is not met. This may mean that it was attempted, but not passed with proficiency, or it may mean that it was never attempted/demonstrated. The outcome is the same, regardless.

At the end of the term, grades will be assigned as follows:

* **Excellent** <br/>
    
    To earn an **A** in {{site.name}}, you must complete **all of the objectives** for the third level of each category. In addition, at least 2/3rds of those objectives (at all levels) must be met with a proficiency of "E". (For clarity, you cannot complete all of the low-level objectives with excellence, and merely demonstrate proficiency at the third level of competency. Excellence is consistent.)

* **Good** <br/> 
    
    To earn a **B** in {{site.name}}, you must complete the all of the second level of competence in each category, and at least half of the third level of competence in each category. Completion of all objectives, where more than 1/3rd are rated as "Proficient" (as opposed to excellent) will result in a "B" as well. (Meaning, if you did not consistently demonstrate excellence in the supermajority of your work, you will earn a B, which is above-average and evidence of hard work.)
    
* **Average** <br/>

    To earn a **C** in {{site.name}}, you must complete all of the second level of competence in each category.
    
* **Poor and Failing** <br/>

    Failure to complete all of the second level of competence in each category will yield a grade of **D** or **F**, as judged by the instructor.
    
Participation accounts for 10% of your final assessment. It is judged by your instructor (possibly in conjunction with your peers); your participation is a reflection of the professionalism and commitment you bring to your learning and the learning of those around you.

{% callout info %}
If the instructor misjudges the amount of time it takes to complete the learning objectives in the course, decisions will be made as to how to adjust the assessment. At no point will you be held accountable for large amounts of work at the last minute, nor will students suffer as a result of misjudgment or poor planning on the part of the instructor.
{% endcallout %}

## Class Policies

These are [detailed on their own page](policies.html). There aren't many, but they're less interesting than the stuff about what you will learn and how you will know you have learned it.

## Class Atmosphere

I want many things for students in my classes, and I very much want you to help me achieve these goals.

* I want our laboratory to be a relaxed environment where you are **comfortable trying new things** and (sometimes) failing. By "failure" I do not mean "receiving an F," but I do mean that you try things, make mistakes, and learn from them. The last bit---learning from our mistakes---is the critical part. Neither I, nor you, nor your classmates should put down or belittle a classmate for trying.

* I want you to **look forward to {{site.short}} because it is fun**. We should be comfortable with each other---humor and laughter makes the day go faster and better. That said...

* We should **work hard, and be proud of that effort**. For me, a "fun" day is one where I've worked hard and improved myself. I have done my best to design a course that will be fun because it challenges us to work hard and do new and interesting things.

* **Respect matters**. Respect for each-other, regardless of where we are from and where we are on our life journey is of utmost importance. I have a great deal of respect for your effort as a student; I show that respect by challenging you to extend your limits, and supporting you to the best of my ability as you take risks and engage with the course throughout the semester.

## Catalogue Description

A study of the theory and techniques necessary for electrical and electronic systems and their associated equipment. Students will learn how to identify, calculate, measure, create, and repair basic electrical and electronic systems. These skills will be applied to a selection of practical projects that will challenge the students understanding of the material and problem solving/troubleshooting abilities. Topics may include AC/DC circuits, resistance, power, various components, and use of electrical measuring instruments. 

## Statement Regarding Disability

Berea College will provide reasonable accommodations for all persons with disabilities so that learning experiences are accessible. If you experience physical or academic barriers based on disability, please see Lisa Ladanyi (Disability & Accessibility Services, 110 Lincoln Hall, 859-985-3327, lisa.ladanyi@berea.edu) to discuss options. Students must provide their instructor(s) with an accommodation letter before any accommodations can be provided. Accommodations cannot be provided retroactively. Please meet with your instructor(s) in a confidential environment to discuss arrangements for these accommodations.

{% callout danger %}
This syllabus is a living document. It may change.
{% endcallout %}
