---
title: Course Goals
resource: invisible
layout: default
---

The course learning goals provide the "big picture" for what we will study this term. They don't spell out individual assignments or particular learning objectives, but instead define the broad areas of study that we will engage in over the course of the term.

## Outline

1. [Safety](#safe)
1. [Communication](#comms)
1. [Design](#design)
1. [Build](#build)

## Course Goals 
If this was the *only* course in electricity and electronics you take at Berea College, and you never touch or study the subject again while you are a student, what questions do I hope you will be able to answer 10 years from now?

1. <a name="safe"></a>**How can I be safe in everything I do?** <br/>

1. <a name="comms"></a>**How do I communicate, with clarity, the results of my efforts?** <br/>

1. <a name="design"></a>**How can I design electrical systems to sense and interact with the world?** <br/>

1. <a name="build"></a>**How do I build and program those systems?** <br/>

{% comment %}
<!-- Should these be the goals instead? -->

* **MAKER**: How do I use the tools of the global electronics maker community to create, communicate, and collaborate on electrical systems?

* **DESIGN**: What knowledge of electronic components (and how they come together) do I need to design circuits to sense and automate the world around me?

* **LANGUAGE**: How do I interpret and create the many textual and visual representations of electronic circuits, and use that information to build circuits that work?

* **CODE**: How can I prepare myself to program systems that sense and automate the world around me, even if I've never programmed a computer before?

* **CONNECTIONS**: What are the connections between the theories of electricity and electronics to our daily lives?

* **QUESTION**: What questions can I ask about myself and the world around me, now and in the future, that sensing and automation systems can help me answer?
{% endcomment %}