---
title: Resources
resource: local
---

## Texts

* **[All About Circuits]({{site.data.urls.texts.allabout}})**

    A free textbook about DC and AC circuitry. Excellent resource.

## WWW
    
* **[learn.sparkfun.com]({{site.data.urls.www.learnsfe}})**

    Resources for learning from Sparkfun.
    
* **[learn.adafruit.com]({{site.data.urls.www.learnada}})**

    Resources for learning from Adafruit Industries.

* **[Electronics Learning Resources]({{site.data.urls.www.elr}})**

    Resources collected by Charles Williams at the University of Exeter.