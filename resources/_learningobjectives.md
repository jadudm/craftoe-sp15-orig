---
title: Learning Objectives
resource: invisible
layout: default
gavel: "<i class='fa fa-fixed fa-gavel'></i>"
---

Learning objectives are the assessable points of learning in the course. The number of these learning objectives you complete is what determines your final grade in this course.

These objectives are subject to change. In particular, the "jury is still out" on objectives marked with a {{page.gavel}}.

## Outline

1. [Craft: Fabrication and Safety](#craft)

1. [Theory of Electronics](#theory)

1. [Components and Build](#build)

1. [Electronics in the World](#world)

## Craft: Fabrication and Safety

<a name="craft"></a>

This material will be covered over the course of the entire term, and will be assessed through quizzes, exams, structured reporting, and direct observation.

### Level 1

1. Demonstrate understanding of basic breadboarding practice, including the use of wire strippers, crimpers, and related tools.

1. Use Fritzing for breadboard diagrams of basic circuits.

1. Identify basic components in schematic form.

### Level 2

1. Demonstrate (through written material and practical demonstration) safe use of a soldering iron.

1. Use Fritzing for schematic capture.

1. Identify basic components in PCB form.

1. Build basic circuits from a schematic.

1. Use a multimeter for basic diagnostics (continuity, voltage, current).

1. Discuss and demonstrate good multimeter safety.

### Level 3

1. Lay out single-sided PCBs in Fritzing and cut on a PCB mill.

1. Use an oscilloscope for basic diagnostics of oscillating circuits. {{page.gavel}}

1. Reverse-engineer circuit schematics from a PCB.

1. Interpret datasheets for basic data (eg. maximum safe voltage and current limits).

## Theory of Electronics

<a name="theory"></a>

### Level 1

1. Use V=IR to calculate whether circuits are safe for components and/or people.

1. Use P=IV to calculate power consumed by a device.

1. Read and interpret basic component values (resistors, capacitors).

1. Know safe limits for voltage and current (for people).

### Level 2

1. Use V=IR and P=IV together to solve for unknowns.

1. Demonstrate understanding of series/parallel reductions for resistance, capacitance, and voltage.

1. Know basic truth tables.

1. Demonstrate parallel/series switch networks and relate them to logic systems.

1. Build and demonstrate working pull-up and pull-down resistor configurations.

### Level 3

1. Build and calculate appropriate voltage dividers for analog sensor use.

1. Understand basic control theories (closed-loop, open-loop, PID). {{page.gavel}}

1. Demonstrate basic statistical knowledge as applied to measurements — mean, median, mode, error, etc.

1. Demonstrate calibration or mapping between values.

## Components and Build

<a name="build"></a>

This category of learning outcomes is subject to a fair bit of revision. Specifically, some of these outcomes are about programming, and it might be nice to see those outcomes in their own category... even if it is only to clean this up a bit.

### Level 1

1. Demonstrate knowledge of basic tools (soldering iron, solder sucker, wire stripper).

1. Demonstrate knowledge of passive components:
    * Resistors
    * Capacitors
    * Diodes

1. Build circuits from multiple kinds of instructions (schematic, text) using passive components.

### Level 2

1. Demonstrate the ability to actuate (on, off) components using code.

1. Demonstrate ability to read the state of binary inputs (eg. buttons) and use conditionals in code.

1. Demonstrate knowledge of switching components:
    * Transistors
    * Solid-state relays
    * Mechanical relays
    * Buttons/DIPs/Switches

### Level 3

1. Demonstrate knowledge of analog input and output

1. Demonstrate the ability to develop subroutines for code reuse. {{page.gavel}}

1. Demonstrate knowledge and use of basic actuators:
    * Servos
    * DC Motors
    * Steppers
    * Solenoids

1. Demonstrate knowledge and use of basic analog sensors:
    * Temperature
    * Light
    * Force
    * Moisture

1. Demonstrate ability to compute over values (write expressions) in code.

## Electronics in the World {{page.gavel}}

<a name="world"></a>

This category is not yet fully developed. Specifically, there is a need in the course to measure students' understanding of the role of electronics in the world today---in terms of sensing, actuation, and what it means to do these things sustainably. In addition, I am interested in students developing a sense for themselves in the field, by considering issues of race and gender in the field... topics which are timely and critical for an electronics student in the liberal arts.


## Habits of Practice

These are not learning objectives *per se*, but instead represent practices that you are expected to engage in all term.

1. Engage in question asking and answering in [Q.berea]({{site.data.urls.qberea}}), our course discussion tool.

1. Get together with your learning team(s) at least once per week outside of class. {{page.gavel}}

1. Journal regarding your learning successes and challenges. (This might also be thought of as a *learning trajectory log*, where you reflect on what objectives you are making progress on, and so on.)

These will likely be incorporated explicitly in assignments.
