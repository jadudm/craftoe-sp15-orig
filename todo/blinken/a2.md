---
---

## Overview

1. Reading (Preface, Chapter 1, Chapter 2 of MGTE)

There are questions to answer at the bottom of this page. You should receive an email after submission containing your answers, for future reference.

---

## Reading (MGTE)

In the Manga Guide to Electronics, please read the preface, chapter one, and chapter two. This would be through page 80 in the text. (You may think 50 pages is a lot of reading, but in this case... it's not.)

I would like you to maintain a notebook for this class; this can be loose-leaf, but make sure you have a single, coherent place for notes for your study of electricity and electronics. I will, on occasion, check these notebooks for particular things. Sometimes, it will be that you made notes; sometimes, that you came up with questions. It will vary... but, making notes about things you think are important, and writing questions about things you are confused about are both important parts of the learning process when you are reading a new text.

In this case, I would ask you to make notes and focus on a few things:

### Chapter One 

* **P, V, and I**. We'll want to be able to work with the relationship between power, voltage, and current. You might take a look at one or two electrical devices and come in prepared to discuss them based on their voltage and current ratings.

* **Voltage/Potential**. Make some notes, or come in with questions. 

* I'm not worried about the atomic structure of things and static electricity... at this time. We'll come back to it. So, read it, make notes as you will, but this isn't going to be a focus for our conversation at this time.

### Chapter Two

* **The idea of switches**. Possibly obvious, possibly not.

* **DC vs. AC**. We will work with DC a lot in this class.

* **Current and Resistance**. Useful relationships; make notes on the analogies and formulas used.

* **Basic Circuits**. Ohm's Law (p. 82) is something we'll work with all term. Might as well start thinking about it, making notes, and asking questions *right now*.

* We won't focus on series and parallel *just yet*. But we will soon.

## Learning Objectives

This assignment moves us towards being able to complete multiple objectives.

*Theory 1.1*: Use V=IR to calculate whether circuits are safe for components and/or people.

*Theory 1.2*: Use P=IV to calculate power consumed by a device.

*Components 1.2*: Demonstrate knowledge of passive components.

## Submission

<div class="text-center">
<script type="text/javascript" src="http://form.jotformpro.com/jsform/50073738915964"></script>
</div>